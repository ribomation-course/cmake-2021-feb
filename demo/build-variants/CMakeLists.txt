cmake_minimum_required(VERSION 3.10)
project(msg LANGUAGES C CXX)

add_executable(hello-cxx hello.cxx)
add_executable(hello-c hello.c)

