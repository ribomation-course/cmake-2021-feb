cmake_minimum_required(VERSION 3.9)
project(using-open-mp LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 			14)
set(CMAKE_CXX_EXTENSIONS 		OFF)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)

find_package(OpenMP REQUIRED)

add_executable(app src/app.cxx)
target_link_libraries(app
	PUBLIC    OpenMP::OpenMP_CXX
)


