#include "numbers.hxx"
#include "gtest/gtest.h"

TEST(sample, simple) {
    ASSERT_EQ(Numbers::fibonacci(10), 55);
}

TEST(sample, base) {
    ASSERT_EQ(Numbers::fibonacci(0), 0);
    ASSERT_EQ(Numbers::fibonacci(1), 1);
    ASSERT_EQ(Numbers::fibonacci(2), 1);
}

