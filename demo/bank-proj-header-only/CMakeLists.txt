cmake_minimum_required(VERSION 3.10)
project(bank-proj 
	VERSION		1
    LANGUAGES	CXX
)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)

add_library(accounts INTERFACE)
target_sources(accounts 
	INTERFACE   ${CMAKE_CURRENT_LIST_DIR}/src/accounts/account.hxx
				${CMAKE_CURRENT_LIST_DIR}/src/accounts/util.hxx
)
target_include_directories(accounts
	INTERFACE   ${CMAKE_CURRENT_LIST_DIR}/src/accounts
)

add_executable(bank 
	${CMAKE_CURRENT_LIST_DIR}/src/bank/bank.cxx
)
target_link_libraries(bank accounts)

