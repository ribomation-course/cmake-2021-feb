#pragma once
#include <string>
#include "util.hxx"
using namespace std;

class Account {
	int balance;
  public:
	Account(int balance): balance{balance} {
		string accno = toUpperCase("hsbc-123-4567");
	}	
	int getBalance() {
		return balance; 
	}
	int update(int amount) {
		balance += amount;
		return balance;
	}
};
