cmake_minimum_required(VERSION 3.10)
project(bank-proj 
	VERSION		1
    LANGUAGES	CXX
)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)

message("baseDir: ${CMAKE_CURRENT_LIST_DIR}")

add_library(accounts "")
target_sources(accounts 
	PUBLIC   ${CMAKE_CURRENT_LIST_DIR}/src/accounts/api/account.hxx
	PRIVATE	 ${CMAKE_CURRENT_LIST_DIR}/src/accounts/impl/account.cxx
             ${CMAKE_CURRENT_LIST_DIR}/src/accounts/impl/util.hxx
             ${CMAKE_CURRENT_LIST_DIR}/src/accounts/impl/util.cxx
)
target_include_directories(accounts
	PUBLIC   ${CMAKE_CURRENT_LIST_DIR}/src/accounts/api
	PRIVATE	 ${CMAKE_CURRENT_LIST_DIR}/src/accounts/impl
)


add_executable(bank 
	${CMAKE_CURRENT_LIST_DIR}/src/bank/bank.cxx
)
target_link_libraries(bank accounts)

