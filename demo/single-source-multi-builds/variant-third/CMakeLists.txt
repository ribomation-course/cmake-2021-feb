
add_executable(app-3rd "")

target_sources(app-3rd PRIVATE 
    ${SRC_DIR}/config.hxx
    ${SRC_COMMON}
)

target_compile_definitions(app-3rd PRIVATE
    VARIANT=3
    CONFIG=1
)

target_include_directories(app-3rd PRIVATE
    ${SRC_DIR}
)

set_target_properties(app-3rd PROPERTIES 
    OUTPUT_NAME app
)

