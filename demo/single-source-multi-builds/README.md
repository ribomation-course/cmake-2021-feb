# Build & Execute

    mkdir bld
    cd bld
    cmake -DCMAKE_BUILD_TYPE=MinSizeRel ..
    cmake --build .
    cmake --build . --target run

N.B. Because of the `preload.cmake` file in the project root, the build tool will default to Ninja.


