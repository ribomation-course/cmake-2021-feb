#include <iostream>
#include <iomanip>
#include <string>
#include "host-info.hxx"
#include "build-info.hxx"
using namespace std;
using namespace std::literals;

int main() {
	cout << "Host name  : " << HostInfo::hostName() << endl;
	cout << "OS   name  : " << HostInfo::osName()   << endl;
	cout << "CPU  name  : " << HostInfo::cpuName()  << endl;
	cout << "64-bit     : " << boolalpha << HostInfo::is64bit()   << endl;
	cout << "RAM size   : " << HostInfo::ramSize()/1024.0 << " GB" << endl;
	
	cout << "User name  : " << BuildInfo::userName() << endl;
	cout << "App name   : " << BuildInfo::appName() << endl;
	cout << "App version: " << BuildInfo::appVersion() << endl;
	cout << "Build date : " << BuildInfo::buildDate() << endl;
	cout << "Commit ID  : " << BuildInfo::commitId() << endl;
	
	cout << BUILD_INFO << endl;
	
	return 0;
}
