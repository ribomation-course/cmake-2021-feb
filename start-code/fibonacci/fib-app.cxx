#include <iostream>
#include <string>
#include "fibonacci.hxx"
using namespace std;

int main(int argc, char** argv) {
	auto N = argc==1 ? 42U : stoi(argv[1]);
	
	cout << "fib(" << N << ") = " << fibonacci(N) << endl;
	
	return 0;
}
