cmake_minimum_required(VERSION 3.16)
project(hello-app)

set(CMAKE_Fortran_COMPILER /usr/bin/gfortran-9)
set(CMAKE_C_STANDARD		99)
set(CMAKE_CXX_STANDARD		17)
set(dialect "-ffree-form -std=f2008 -fimplicit-none")
set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${dialect}")


enable_language(C CXX Fortran)

add_executable(hello-c   src/hello.c)
add_executable(hello-cxx src/hello.cxx)
add_executable(hello-f   src/hello.f90)

add_custom_target(run
	COMMAND hello-f
	COMMAND hello-c
	COMMAND hello-cxx
)


# mkdir bld && cd bld && cmake .. && cmake --build . && cmake --build . --target run
