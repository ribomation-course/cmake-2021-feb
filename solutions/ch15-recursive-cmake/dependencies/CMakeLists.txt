include(FetchContent)

FetchContent_Declare(yamlcpp
    GIT_REPOSITORY  https://github.com/jbeder/yaml-cpp.git
    GIT_TAG         yaml-cpp-0.6.2
)
FetchContent_GetProperties(yamlcpp)
if(NOT yamlcpp_POPULATED)
    message("Downloading YaML++...")
    FetchContent_Populate(yamlcpp)    
    message("Downloading YaML++...DONE")
    add_subdirectory(${yamlcpp_SOURCE_DIR} ${yamlcpp_BINARY_DIR})
endif()

set(YAMLCPP_INCLUDE_DIR  ${yamlcpp_SOURCE_DIR}/include        PARENT_SCOPE)
set(YAMLCPP_LIBFILE      ${yamlcpp_BINARY_DIR}/libyaml-cpp.a  PARENT_SCOPE)

