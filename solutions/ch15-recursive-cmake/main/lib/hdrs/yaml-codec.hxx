#pragma once
#include <vector>
#include <map>
#include <string>
using namespace std;

struct YamlCodec {
    string toYaml(vector<string> list);
    string toYaml(map<string, string> object);
};

