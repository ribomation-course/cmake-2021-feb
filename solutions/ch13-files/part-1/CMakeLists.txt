cmake_minimum_required(VERSION 3.12)
project(files LANGUAGES	NONE)


file(WRITE date.sh [=[  
#!/usr/bin/env bash
set -x
date
]=])

file(COPY date.sh DESTINATION tmp)

execute_process(
	COMMAND ./date.sh
	WORKING_DIRECTORY	${CMAKE_BINARY_DIR}/tmp
	OUTPUT_VARIABLE		currentDate
)

message(${currentDate})

# mkdir bld && cd bld && cmake ..
