cmake_minimum_required(VERSION 3.12)
project(files LANGUAGES	NONE)

set(readmeUrl https://gitlab.com/ribomation-course/cmake-2021-feb/-/raw/master/README.md)
set(readmeFile ${CMAKE_BINARY_DIR}/readme.md)

file(DOWNLOAD ${readmeUrl} ${readmeFile} SHOW_PROGRESS)
message("fetched ${readmeFile}")

file(STRINGS ${readmeFile} readmeLines REGEX .*course.*)

foreach(line ${readmeLines})
	message("line: ${line}")
endforeach()

list(LENGTH readmeLines readmeCount)
message("------------------")
message("Num lines: ${readmeCount}")

# mkdir bld && cd bld && cmake ..
# grep -c course readme.md
