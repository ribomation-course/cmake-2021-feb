cmake_minimum_required(VERSION 3.12)
project(shapes LANGUAGES CXX)

add_executable(shapes
    shape.hxx shape.cxx 
    rect.hxx rect.cxx
    square.hxx square.cxx
    triangle.hxx triangle.cxx
    circle.hxx circle.cxx 
    shape-generator.hxx shape-generator.cxx 
    shapes-app.cxx
)
