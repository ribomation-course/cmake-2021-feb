#pragma once
#include <string>
#include <stdexcept>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

namespace ribomation::shm {
    using namespace std;
    using namespace std::literals;

    class SharedMemory {
        const string    name = "/myshm"s;
        const unsigned  size;
        const void*     begin;
        const void*     end;
        void* next;

        void sysfail(const string& func) {
			string msg = func + "() failed: " + strerror(errno);
			throw runtime_error(msg);
		}

    public:
        SharedMemory(unsigned size) : size{size} {
			int fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0660);
			if (fd < 0) sysfail("shm_open");

			int rc = ftruncate(fd, size);
			if (rc < 0) sysfail("ftruncate");

			auto start = mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
			if (start == MAP_FAILED) sysfail("mmap");
			close(fd);

			SharedMemory::begin = start;
			SharedMemory::end   = start + size;
			SharedMemory::next  = start;
		}
		
        ~SharedMemory() {
			void* addr = const_cast<void*>(begin);
			munmap(addr, size);
			shm_unlink(name.c_str());
		}

        void* allocateBytes(unsigned numBytes) {
			void* addr = next;
			next += numBytes;
			if (end < next) {
				throw overflow_error("shared-memory overflow");
			}
			return addr;
		}

        template<typename T>
        T* allocate(unsigned numElems=1) {
            return reinterpret_cast<T*>(allocateBytes(numElems * sizeof(T)));
        }

        SharedMemory() = delete;
        SharedMemory(const SharedMemory&) = delete;
        SharedMemory& operator=(const SharedMemory&) = delete;
    };

}
