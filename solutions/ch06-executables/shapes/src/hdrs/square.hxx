#pragma once
#include "rect.hxx"

class Square : public Rect {
public:
    explicit Square(int side) : Rect("square", side, side) { }
    ~Square() override;
};
